# soal-shift-sisop-modul-2-D08-2022


|          Nama           |      NRP   |
| :-----------------------| :----------|
| Theresia Nawangsih      | 5025201144 |
| Thoriq Fatihassalam     | 5025201254 |
| Handitanto Herprasetyo | 5025201077 |
|                         |            |



# Soal 1
Pada soal ini diminta untuk membuat simulasi sistem gatcha dimana program dapat mendownload dan mengekstrak file characters dan weapon yang akan digunakan sebagai database. Data yang diambil pada setiap kali gacha dilakukan secara bergantian, gacha bernilai genap mengeluarkan 'weapon' dan gacha bernilai ganjil mengeluarkan 'characters'. Kemudian akan dibuat sebuah folder dengan nama 'gacha_gacha' untuk menyimpan semua hasil gacha. Setiap kali gacha-nya mod 10 maka akan dibuat file baru (.txt) dan setiap gacha-nya mod 90 maka akan dibuat folder baru dan file(.txt) yang berisi 10 hasil gacha dengan format penamaan file {Hh:Mm:Ss}_gacha_{jumlah-gacha} dan penamaan folder total_gacha_{jumlah-gacha}. Untuk melakukan gacha dibutuhkan alat tukar berupa 'primogems' sejumlah 160 primogems. Setiap kali gacha terdapat 2 properties yaitu name dan rarity lalu outputnya berupa file(.txt) dengan format {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}. Gacha akan dilakukan hingga primogems habis. Gacha akan dimulai pada 30 Maret jam 04:44. Kemudian semua isi di folder 'gacha_gacha' akan di zip dengan nama 'not_safe_for_wibu' dengan password 'satuduatiga'.

## Download file dan ekstrak
```
 void downloadExtract()
{
    char *urls[] = {"https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download",
                    "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"};

    char *filename[] = {"characters.zip", "weapons.zip"};
    id_t child_id;
    int status;

    if ((child_id = fork()) == 0)
    {
        execlp("mkdir", "mkdir", "-p", "gacha_gacha", NULL);
    }

    for (int i = 0; i < 2; i++)
    {
        if ((child_id = fork()) == 0)
        {
            execlp("wget", "wget", "--no-check-certificate", urls[i], "-O", filename[i], "-q", NULL);
        }

        while ((wait(&status)) > 0)
            ;

        if ((child_id = fork()) == 0)
        {
            execlp("unzip", "unzip", "-qq", filename[i], NULL);
        }

        while ((wait(&status)) > 0)
            ;

        if ((child_id = fork()) == 0)
        {
            execlp("rm", "rm", "-f", filename[i], NULL);
        }
        while ((wait(&status)) > 0)
            ;
    }
}
```

## Gacha

Simulasi gacha dengan working directory 'gacha_gacha'
```
void gacha(int gacha_sim)
{
    id_t child_id;
    int status;
    if (gacha_sim == 0)
    {
        if ((chdir("gacha_gacha")) < 0)
        {
            exit(EXIT_FAILURE);
        }
    }

```
Setiap gacha-nya mod 10 maka akan dibuat file (.txt) baru
```
    if (gacha_sim % 10 == 0)
    {
        char r_dir_name[30] = "";
        strcat(r_dir_name, dir_name);
        if ((chdir(r_dir_name)) < 0)
        {
            exit(EXIT_FAILURE);
        }
        srand(time(NULL));
        for (int i = 1; i < 10; i++)
        {
            char file_name[25], buffer2[10];
            struct tm *formattedTime = formatTime(myTime, 1);
            strftime(file_name, sizeof(file_name), "%H:%M:%S", formattedTime);
            strcat(file_name, "_gacha_");
            snprintf(buffer2, 10, "%d", (i * 10) + gacha_sim);
            strcat(strcat(file_name, buffer2), ".txt");
```

Setiap gacha-nya mod 90 maka akan dibuat folder baru dan file (.txt)

```
if (gacha_sim % 90 == 0)
    {
        if ((child_id = fork()) == 0)
        {
            execlp("mkdir", "mkdir", "-p", dir_name, NULL);
        }

        while ((wait(&status)) > 0)
            ;
    }
```
Setiap kali gacha bernilai genap maka mengeluarkan item weapons dan setiap kali gacha bernilai ganjil maka mengeluarkan item characters

```
                if (j % 2 != 0)
                {
                    char json_path[70] = "../../characters/";
                    strcat(json_path, get_random_file("../../characters", rand() % (48 - 1) + 1));
                    fprintf(fp, "%d_characters_%s_%d\n", (j) + (i * 10) + gacha_sim - 10, file_to_json(json_path), primo_gems);
                }
                else
                {
                    char json_path[70] = "../../weapons/";
                    strcat(json_path, get_random_file("../../weapons", rand() % (130 - 1) + 1));
                    fprintf(fp, "%d_weapons_%s_%d\n", (j) + (i * 10) + gacha_sim - 10, file_to_json(json_path), primo_gems);
                }
```

## Penamaan File

Format penamaan setiap file (.txt) adalah {Hh:Mm:Ss}_gacha_{jumlah-gacha} dan format penamaan setiap foldernya adalah total_gacha_{jumlah-gacha}

Pembuatan struct formatWaktu untuk
```
struct tm *formatWaktu(struct tm *inputWaktu, int addition)
{
    if (inputWaktu->tm_sec + addition >= 60)
    {
        inputWaktu->tm_min++;
        inputWaktu->tm_sec = addition;
        if (inputWaktu->tm_min >= 60)
        {
            inputWaktu->tm_hour++;
            inputWaktu->tm_min = 0;
            if (inputWaktu->tm_hour >= 24)
            {
                inputWaktu->tm_hour = 0;
            }
        }
    }
    else
    {
        inputWaktu->tm_sec += addition;
    }
    return inputWaktu;
}
```

Kemudian membuat penamaan file sesuai format

```
        for (int i = 1; i < 10; i++)
        {
            char file_name[25], buffer2[10];
            struct tm *formattedTime = formatTime(myTime, 1);
            strftime(file_name, sizeof(file_name), "%H:%M:%S", formattedTime);
            strcat(file_name, "_gacha_");
            snprintf(buffer2, 10, "%d", (i * 10) + gacha_counter);
            strcat(strcat(file_name, buffer2), ".txt");

            if ((child_id = fork()) == 0)
            {
                execlp("touch", "touch", file_name, NULL);
            }
            while ((wait(&status)) > 0)
                ;
```

## Mekanisme gacha

Primo gems pada awal di-define sebanyak 79000 primogems. Primogems akan berkurang sebanyak 160 setiap kali melakukan gacha.

```
            int primo_gems;
            FILE *fp;
            fp = fopen(file_name, "a");
            for (int j = 1; j <= 10; j++)
            {
                primo_gems = 79000 - 160 * ((j) + (i * 10) + gacha_sim - 10);
                if (primo_gems < 0)
                    break;
                if (j % 2 != 0)
                {
                    char json_path[70] = "../../characters/";
                    strcat(json_path, get_random_file("../../characters", rand() % (48 - 1) + 1));
                    fprintf(fp, "%d_characters_%s_%d\n", (j) + (i * 10) + gacha_sim - 10, file_to_json(json_path), primo_gems);
                }
                else
                {
                    char json_path[70] = "../../weapons/";
                    strcat(json_path, get_random_file("../../weapons", rand() % (130 - 1) + 1));
                    fprintf(fp, "%d_weapons_%s_%d\n", (j) + (i * 10) + gacha_sim - 10, file_to_json(json_path), primo_gems);
                }
            }
            fclose(fp);
            if (primo_gems < 0)
            {
                return;
            }
```

## Waktu gacha, hasil gacha, dan penghapusan file

Gacha dimulai pada 30 Maret jam 04:44 dan 3 jam kemudian folder 'gacha_gacha' akan di zip dengan nama 'not_safe_for_wibu' dan dikunci dengan password 'satuduatiga'. Semua folder kemudian didelete dan hanya menyisakan file zip.

```
 while (1)
    {
        id_t child_id;
        int status;

        time_t currentTime;
        time(&currentTime);
        struct tm *currTime = localtime(&currentTime);
        char time_var[15];
        strftime(time_var, sizeof(time_var), "%H:%M:%S_%d-%m", currTime);
        if (strcmp(time_var, "04:44:00_30-03") == 0)
        {
            int primo = 79000, i = 0;
            downloadExtract();
            while (primo > 0)
            {
                gacha(i);
                i += 90;
                primo -= 14400;
            }
        }
```
Mengunci file zip

```
        else if (strcmp(time_var, "07:44:00_30-03") == 0)
        {
            if ((chdir("../../")) < 0)
            {
                exit(EXIT_FAILURE);
            }
            char cwd[PATH_MAX];
            if (getcwd(cwd, sizeof(cwd)) != NULL)
            {
                printf("Current working dir: %s\n", cwd);
            }
            else
            {
                perror("getcwd() error");
            }
            if ((child_id = fork()) == 0)
            {
                execlp("zip", "zip", "-P", "satuduatiga", "-r", "not_safe_for_wibu", "-q", "gacha_gacha", NULL);
            }
```

Menghapus semua file dan menyisakan file zip

```
            if ((child_id = fork()) == 0)
            {
                execlp("rm", "rm", "-r", "gacha_gacha", NULL);
            }

            while ((wait(&status)) > 0)
                ;

            if ((child_id = fork()) == 0)
            {
                execlp("rm", "rm", "-r", "characters", NULL);
            }
            while ((wait(&status)) > 0)
                ;

            if ((child_id = fork()) == 0)
            {
                execlp("rm", "rm", "-r", "weapons", NULL);
            }

            while ((wait(&status)) > 0)
                ;
```


# Soal 3
Pada soal ini diminta untuk membuat 2(dua) directory dengan nama `darat` dan 3 detik kemudian membuat directory dengan nama `air`, setelah itu melakukan extract `animal.zip`. Hasil extract akan dipisah menjadi `hewan darat` dan `hewan air` sesuai dengan nama file dan dimasukkan pada folder masing-masing. Untuk hewan yang tidak memiliki keterangan makan akan terhapus. Setelah berhasil memisahkan hewan berdasarkan dengan kategori `air` dan `darat`, karena pada kebun binatang memiliki jumlah burung yang terlalu banyak makan burung akan terhapus pada directory `darat`, hewan burung ditandai dengan nama file `bird`. Terakhir, membuat file `list.txt` pada folder `air`  dengan format `UID_[UID file permission]_Nama File.[jpg/png]`.

## Membuat Directory Darat dan Air
Untuk membuat directory ini menggunakan `fork`, `exec`, dan `wait`
## Darat
```
id_t child_id;
    int status;
    if((child_id=fork()) == 0) 
    {
        char *argv[] = {"mkdir", "/home/theresianwg/modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    }
    
    while((wait(&status)) >= 1);
```

## Air
menggunakan `sleep(3)` dikarenakan pada soal terdapat kriteria membuat folder air 3 detik setelah folder darat. 
```
 if((child_id=fork()) == 0) 
    {
        sleep(3);
        char *argv[] = {"mkdir", "/home/theresianwg/modul2/air", NULL};
        execv("/bin/mkdir", argv);
    }
    
    while((wait(&status)) >= 1);
```

## Melakukan Extract animal.zip
Pada kali ini sama dengan menggunakan `fork`, `exec`, dan `wait`.

```
if((child_id=fork()) == 0) 
    {
        char *argv[] = {"unzip", "-qq","/home/theresianwg/modul2/animal.zip", NULL};
        execv("/usr/bin/unzip", argv);
    }
    
    while((wait(&status)) >= 1);
```
`((wait(&status))` digunakan untuk menghindari exit function sebelum script selesai dijalankan.


- Tampilan ketika berhasil membuat folder `darat`, `air`, dan juga extract `animal.zip`
    
    ![berhasil folder](https://drive.google.com/uc?export=view&id=1TLjZGXehzIN1tjE3c7PwOCC68PQ4YcH3)

## Memisahkan Hewan Berdasarkan Jenisnya
Membuka folder animal dengan menggiterasi dengan kondisi tidak NULL. Melakukan copy file dengan `ep->d_name` pada array hewan dan juga menambahkan variabel count yaitu `[50]` untuk menghitung jumlah file yang ada pada folder.

```
void list_and_sort(char *path)
DIR *dp;
    struct dirent *ep;
    dp = opendir(path);
    
    id_t child_id;
    int status;
    
    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
          if(strstr(ep->d_name, "darat")){
            if((child_id=fork()) == 0) 
            {
                char filename[50];
                sprintf(filename, "/home/theresianwg/modul2/animal/%s", ep->d_name);
                char *argv[] = {"cp", filename, "/home/theresianwg/modul2/darat", NULL};
                execv("/bin/cp", argv);
            }
            while((wait(&status)) > 0);
            
          }
          else if(strstr(ep->d_name, "air")){
            if((child_id=fork()) == 0) 
            {
                char filename[50];
                sprintf(filename, "/home/theresianwg/modul2/animal/%s", ep->d_name);
                char *argv[] = {"cp", filename, "/home/theresianwg/modul2/air", NULL};
                execv("/bin/cp", argv);
            }
            while((wait(&status)) > 0);
            
          }
      }
      (void) closedir (dp);
    } else  printf("Couldn't open the directory");
```
jika tidak dapat membuka directory maka akan muncul outpu pesan `"Couldn't open the directory"`

- Tampilan isi folder `darat` dan `air`

* `darat`

    ![darat](https://drive.google.com/uc?export=view&id=19NeQGoUPU41Sox7ksLT0d2-09j_0v3YT)

* `air`

    ![air](https://drive.google.com/uc?export=view&id=1pOwe4hxwz4hn4Jf5MLFjeFsIAGwRUzGL)


## Menghapus Semua Hewan Burung pada Folder Darat
Pertama, membuka folder darat `/home/theresianwg/modul2/darat` lalu untuk menghapus file burung dengan menggunakan `execv("/bin/rm"` dan juga melakukan copy file `bird` menggunakan `ep->d_name, "bird"`. `while((wait(&status))` untuk dapat menghindari exit function sebelum script selesai dijalankan.

```
void bird_filter(char *path){
    DIR *dp;
    struct dirent *ep;
    dp = opendir(path);
    
    id_t child_id;
    int status;
    
    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
          if(strstr(ep->d_name, "bird")){
            if((child_id=fork()) == 0) 
            {
                char filename[50];
                sprintf(filename, "/home/theresianwg/modul2/darat/%s", ep->d_name);
                char *argv[] = {"rm", filename, NULL};
                execv("/bin/rm", argv);
            }
            while((wait(&status)) > 0);
          }
      }
      (void) closedir (dp);
    } else  printf("Couldn't open the directory");

}
```
jika tidak dapat membuka directory maka akan muncul outpu pesan `"Couldn't open the directory"`

## Membuat file List.txt
Dengan menggunakan `char *permission_filter(char *path)` dan juga `r` adalah untuk `read` dan `w` adalah `write`
```
    struct stat fs;
    int r;
    r = stat(path, &fs);
    if( r==-1 )
    {
        exit(1);
    }

    char perm[15]="\0", *buffer;
	buffer = malloc(sizeof(char)*100);
    
    if( fs.st_mode & S_IRUSR )
        strcat(perm, "r");
    else strcat(perm, "-");
    if( fs.st_mode & S_IWUSR )
        strcat(perm, "w");
    else strcat(perm, "-");
    if( fs.st_mode & S_IXUSR )
        strcat(perm, "x");
    else strcat(perm, "-");


    if( fs.st_mode & S_IRGRP )
        strcat(perm, "r");
    else strcat(perm, "-");
    if( fs.st_mode & S_IWGRP )
        strcat(perm, "w");
    else strcat(perm, "-");
    if( fs.st_mode & S_IXGRP )
        strcat(perm, "x");
    else strcat(perm, "-");

    if( fs.st_mode & S_IROTH )
        strcat(perm, "r");
    else strcat(perm, "-");
    if( fs.st_mode & S_IWOTH )
        strcat(perm, "w");
    else strcat(perm, "-");
    if( fs.st_mode & S_IXOTH )
        strcat(perm, "r");
    else strcat(perm, "-");
            
	sprintf(buffer, "%s", perm);
    return buffer;
```
dilanjutkan dengan
```
if((child_id=fork()) == 0) 
    {
        sleep(3);
        char *argv[] = {"touch", "/home/theresianwg/modul2/air/list.txt", NULL};
        execv("/bin/touch", argv);
    }
    
    while((wait(&status)) >= 1);
    
    list_to_txt("air");
```
`while((wait(&status))` untuk dapat menghindari exit function sebelum script selesai dijalankan.

- Tampilan isi `list.txt`
![list](https://drive.google.com/uc?export=view&id=1ry6ItN3RlWDmbiYpDUVWw02K7lEFlL1C)

