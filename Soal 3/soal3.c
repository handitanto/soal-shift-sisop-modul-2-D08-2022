#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <stdio.h>
#include <pwd.h>
#include <grp.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>

void list_and_sort(char *path){
    DIR *dp;
    struct dirent *ep;
    dp = opendir(path);
    
    id_t child_id;
    int status;
    
    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
          if(strstr(ep->d_name, "darat")){
            if((child_id=fork()) == 0) 
            {
                char filename[50];
                sprintf(filename, "/home/theresianwg/modul2/animal/%s", ep->d_name);
                char *argv[] = {"cp", filename, "/home/theresianwg/modul2/darat", NULL};
                execv("/bin/cp", argv);
            }
            while((wait(&status)) > 0);
            
          }
          else if(strstr(ep->d_name, "air")){
            if((child_id=fork()) == 0) 
            {
                char filename[50];
                sprintf(filename, "/home/theresianwg/modul2/animal/%s", ep->d_name);
                char *argv[] = {"cp", filename, "/home/theresianwg/modul2/air", NULL};
                execv("/bin/cp", argv);
            }
            while((wait(&status)) > 0);
            
          }
      }
      (void) closedir (dp);
    } else  printf("Couldn't open the directory");

}
void bird_filter(char *path){
    DIR *dp;
    struct dirent *ep;
    dp = opendir(path);
    
    id_t child_id;
    int status;
    
    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
          if(strstr(ep->d_name, "bird")){
            if((child_id=fork()) == 0) 
            {
                char filename[50];
                sprintf(filename, "/home/theresianwg/modul2/darat/%s", ep->d_name);
                char *argv[] = {"rm", filename, NULL};
                execv("/bin/rm", argv);
            }
            while((wait(&status)) > 0);
          }
      }
      (void) closedir (dp);
    } else  printf("Couldn't open the directory");

}

char *permission_filter(char *path){
    struct stat fs;
    int r;
    r = stat(path, &fs);
    if( r==-1 )
    {
        exit(1);
    }

    char perm[15]="\0", *buffer;
	buffer = malloc(sizeof(char)*100);
    
    if( fs.st_mode & S_IRUSR )
        strcat(perm, "r");
    else strcat(perm, "-");
    if( fs.st_mode & S_IWUSR )
        strcat(perm, "w");
    else strcat(perm, "-");
    if( fs.st_mode & S_IXUSR )
        strcat(perm, "x");
    else strcat(perm, "-");


    if( fs.st_mode & S_IRGRP )
        strcat(perm, "r");
    else strcat(perm, "-");
    if( fs.st_mode & S_IWGRP )
        strcat(perm, "w");
    else strcat(perm, "-");
    if( fs.st_mode & S_IXGRP )
        strcat(perm, "x");
    else strcat(perm, "-");

    if( fs.st_mode & S_IROTH )
        strcat(perm, "r");
    else strcat(perm, "-");
    if( fs.st_mode & S_IWOTH )
        strcat(perm, "w");
    else strcat(perm, "-");
    if( fs.st_mode & S_IXOTH )
        strcat(perm, "r");
    else strcat(perm, "-");
            
	sprintf(buffer, "%s", perm);
    return buffer;
}

void list_to_txt(char *path){
    DIR *dp;
    FILE *fp;
    fp = fopen("/home/theresianwg/modul2/air/list.txt", "a");
    struct dirent *ep;
    dp = opendir(path);
    
    struct stat info;
    int r;
    
    r = stat(path, &info);
    if(r==-1){
        fprintf(stderr, "Error\n");
        exit(1);
    }
    
    struct passwd *pw = getpwuid(info.st_uid);
    struct group *gr = getgrgid(info.st_gid);
    
    id_t child_id;
    int status;
    
    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
	if((strcmp(ep->d_name,".") && strcmp(ep->d_name,"..")) && strcmp(ep->d_name, "list.txt")){
                char line_name[50] = "\0";
                strcat(line_name, "/home/theresianwg/modul2/air/");
                strcat(line_name, ep->d_name);
                fprintf(fp, "%s_%s_%s\n", pw->pw_name, permission_filter(line_name), ep->d_name);
        }
      }
      (void) closedir (dp);
      fclose(fp);
    } else  printf("Couldn't open the directory");

}

int main() {
    id_t child_id;
    int status;
    if((child_id=fork()) == 0) 
    {
        char *argv[] = {"mkdir", "/home/theresianwg/modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    }
    
    while((wait(&status)) >= 1);
    
    if((child_id=fork()) == 0) 
    {
        sleep(3);
        char *argv[] = {"mkdir", "/home/theresianwg/modul2/air", NULL};
        execv("/bin/mkdir", argv);
    }
    
    while((wait(&status)) >= 1);
    
    if((child_id=fork()) == 0) 
    {
        char *argv[] = {"unzip", "-qq","/home/theresianwg/modul2/animal.zip", NULL};
        execv("/usr/bin/unzip", argv);
    }
    
    while((wait(&status)) >= 1);

	list_and_sort("animal");
    bird_filter("darat");
    
    if((child_id=fork()) == 0) 
    {
        sleep(3);
        char *argv[] = {"touch", "/home/theresianwg/modul2/air/list.txt", NULL};
        execv("/bin/touch", argv);
    }
    
    while((wait(&status)) >= 1);
    
    list_to_txt("air");
 
  
}
